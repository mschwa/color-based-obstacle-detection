# Installation

1. Open the terminal and `cd` into the directory where this project should be cloned;
2. Clone the project: `git clone https://mschwa@bitbucket.org/mschwa/color-based-obstacle-detection.git`;
3. Enter the project directory: `cd ./color-based-obstacle-detection`;
4. Create a virtual environment on the root directory: `python3 -m venv ~/bism`. All project's dependencies will be installed in this environment;
5. Activate the virtual environment just created: `source ~/bism/bin/activate`;
6. Upgrade Python's package installer: `pip install --upgrade pip`;
7. Install project dependencies: `pip install -r requirements.txt` (this could take a while);
8. Install the `sox` library required for audio feedback generation in Ubuntu: `sudo apt install sox`.

# Calibration

0. This procedure should ideally be performed in or close to a lawn or another area with grass;
1. Activate the project's virtual environment if it is not yet active: `source ~/bism/bin/activate`;
2. Access the calibration directory: `cd calibration`;
3. Open the `calibration.json` file in a text editor;
4. Activate the project's virtual environment if it has not yet been activated: `source ~/bism/bin/activate`;
5. Find the key-value pair with key `cam_index` (should be the first key-value pair in the JSON). This key should be associated with a value representing the id of the video capturing device (camera) used, a number usually associated with the USB port to which the camera is connected. For now, don't change it;
6. Run the calibration client: `python index.py`. If the code crashes, with error `Can't open camera by index x`, change the camera id in the key-value pair found in step 4. Possible ids usually range from `0` to `4`. Try a different value and save the JSON file. Then try running `python index.py` again. If the code runs, proceed with calibration; otherwise, try a new value for the camera id until the code runs successfully.

Once the the program starts running you will be able to perform 3 calibration procedures. Before each one, a prompt will be printed to the terminal asking if you would like to go through the next procedure. If you choose to accept it, a message will be printed with further explanation about the procedure.

At the beginning of each procedure, two windows will pop up. One contains the camera stream. The other, which will be referred to as **calibration window**, will contain sliders and a modified version of the camera stream that allows for better visualization of the calibration results.

Note that the calibration procedures listed below may have to be repeated in order to find the parameters that work best for an user.

## Color Calibration

The purpose of the first procedure is to calibrate the hue and saturation values for the thresholding algorithm. A proper combination of the parameters in question will cause all green pixels in the camera stream to be displayed as white in the calibration window stream, and all non-green pixels to be displayed as black.

1. When the two windows pop up, the calibration window stream will be initially blank. This window will also have 4 sliders: `Low H`, `High H`, `Low S` and `High S`. These sliders allow experimenting with different lower and upper limits for hue and saturation ranges used in thresholding. This procedure should define the saturation and hue ranges comprising the green color as captured by the camera;
2. To do so, adjust the sliders to find hue and saturation ranges that produce the following effect: all green areas in the original stream correspond to white areas in the thresholded stream, whereas all non-green areas in the original stream are black in the calibration stream. Example values that worked for Logitech's c525 camera are: `Low H = 10`, `High H = 118`, `Low S = 27` and `High S = 255`;
3. Note that small black regions may appear flickering within larger white areas in the calibration window. These black components are usually associated with shadows, leaves and other small non-green objects that should not be considered as obstacles. To get rid of (most of) them, adjust the value in the trackbar labeled 'Obstacle Min Size' to the **lowest** value that eliminates the flickering as much as possible.
4. Once you have found suitable values, press the `ENTER` key to continue to the next step of calibration.

## Region of Interest (ROI) Calibration

The purpose of the second calibration procedure is to inform to the algorithm what portions of the captured stream fall in the mower's path, i.e. directly in front of it.

1. Only two slider bars will be present in the calibration window. They allow delimiting the left and right bounds of the mower's path respectively;
2. Adjust the `Mower's Left Side` slider so that the left vertical line is positioned just before the mower's left end (when approaching the mower *from the left*);
3. Adjust the `Mower's Right Side` slider so that the right vertical line is positioned just before the mower's right end  (when approaching the mower *from the right*);
4. Once the vertical lines have been positioned appropriately, press the `ENTER` key to proceed to the last step of calibration.

## Beep Zones Calibration

The purpose of the final calibration procedure is to delimit for the algorithm the regions that are very close (~2 steps from the mower), at a medium distance (~2-5 steps from the mower) and far from the mower.

1. Adjust the camera's angle so that bottom of recorded frame is right in front of mower;
2. The calibration window contains 2 sliders `Top Boundary` and `Lower Boundary` that control the height of the upper and lower horizontal red lines respectively. There are also 3 labels displayed in the calibration stream: `NO BEEP ZONE`, `LOW FREQUENCY ZONE`, `HIGH FREQUENCY ZONE`. The area labeled as `NO BEEP ZONE` goes from the top of the frame to the upper red line. The `LOW FREQUENCY ZONE` represents the area in between the two red lines. Finally, the `HIGH FREQUENCY ZONE` consists of the region between the lower red line and the bottom of the frame.
3. Adjust the height of these lines with the following rules of thumb in mind:
    - Objects within the `HIGH FREQUENCY ZONE` should be reachable by the mower in very short time - objects in it pose a high risk of collision for the mower;
    - Objects in the `LOW FREQUENCY ZONE` pose a moderate risk of collision with the mower (collision could happen in the near future if mower continues advancing in its path);
    - Objects in the `NO BEEP ZONE` pose no risk of collision for the mower currently.
4. Once the bounds have been defined, press `ENTER` to conclude the calibration and save all settings.



# Running the Obstacle Detection

0. Exit the calibration directory if you are in it: `cd ..`;
1. Activate the virtual environment if it has not yet been activated: `source ~/bism/bin/activate`;
2. Plug your (2-channel) headphones into the computer making sure the left headphone is in your left ear and the right headphone, in your right ear;
3. Run the program: `python demo.py`.


# Next Tasks

- Investigate how drop rate influences processing speed;
- Research standard procedures for color calibration for automatic adjustment to different light conditions;
- Debug mean shift for hue and saturation thresholds;
- Verify if there's any unecessary operation in preprocessing;
- Create GUI for calibration using Tkinter (see [this post](https://www.pyimagesearch.com/2016/05/23/opencv-with-tkinter/) for reference)