import os
import cv2
import numpy as np
import math
from time import time
from datetime import datetime
from multiprocessing import Process, Manager

# project imports
from preprocess import preprocess
from segment import segment
from audio import notification_routine, segmentation2audio
from constants import LOW_H, HIGH_H, LOW_S, HIGH_S, \
                      CAM_IDX, SIZE, FRAME_W, PROCESSED_W, \
                      PROCESSED_AREA_COL_START, PROCESSED_AREA_COL_END

# create notification process
beep_process_input_manager = Manager()
beep_process_input = beep_process_input_manager.list()
notification_process = Process(target=notification_routine, 
                               args=[beep_process_input])
notification_process.start()

# choose video to read
video_name = 'lawn6.mp4'
# video_name = 'stream_processed.avi'
video_path = 'videos/{}'.format(video_name)

# create input video stream
cap = cv2.VideoCapture(CAM_IDX)
W = int(cap.get(3))
H = int(cap.get(4))
FPS = cap.get(cv2.CAP_PROP_FPS)

# create output video stream
fourcc = cv2.VideoWriter_fourcc(*'MJPG')
# get current time_stamp
ts = datetime.fromtimestamp(time()).strftime('%Y-%m-%d %H:%M:%S')
# pass dimensions as width, height if video is rotated
output_path = './output/test_{}.avi'.format(ts)
writer = cv2.VideoWriter(output_path, fourcc, FPS, (FRAME_W + PROCESSED_W, H), True)

# declare hsv ranges for color thresholding
hsv_min = (LOW_H, LOW_S, 0)
hsv_max = (HIGH_H, HIGH_S, 255)


def detect_obstacles():
    # declare frame counter
    i = 0 
    # store last rate and command generated
    prev_settings = None
    # run demo
    while(cap.isOpened()):
        ret, frame = cap.read()
        # break out of loop if all frames have been read
        if not ret: break
        # rotate frame to original orientation
        # frame = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)

        # drop frame sides
        interest_frame = frame[:, PROCESSED_AREA_COL_START:PROCESSED_AREA_COL_END, :]

        # preprocess frame (BGR colorspace)
        preprocessed_frame = preprocess(interest_frame)
        segmented_frame, obstacle_coordinates = segment(preprocessed_frame, hsv_min, hsv_max, SIZE)

        # segmented_frame, obstacle_coordinates = segment(frame)

        # retrieve audio generation settings
        notification_settings, feedback_frame = segmentation2audio(segmented_frame, obstacle_coordinates)

        # avoid input/process bottlenecking by only passing rates/commands
        # different from the ones it is currently using
        if (notification_settings != prev_settings) and len(beep_process_input) == 0:
            # pass settings to notification process
            beep_process_input.append(notification_settings)
            # upadte previous settings
            prev_settings = notification_settings

        # # display frames
        cv2.imshow("Original", frame)
        #cv2.imshow("Region of interest", interest_frame)
        # cv2.imshow("Preprocessed", preprocessed_frame)
        # cv2.imshow("Segmented", segmented_frame)
        cv2.imshow("Feedback", feedback_frame)
        cv2.waitKey(30)
        # i += 1
        # save specific frames
        # if (i == 343):
        #     cv2.imwrite('./original.jpg', preprocessed_frame)
        #     cv2.imwrite('./segmented.jpg', segmented_frame)
        # print(i)
        # save frame to video being recorded
        writer.write(np.concatenate((frame, feedback_frame), axis=1))

    # close streams
    cap.release()
    writer.release()
    notification_process.terminate()

if __name__ == '__main__':
    detect_obstacles()

    # frame = cv2.imread('./videos/webcam_masked.jpg')
    # hsv_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # h, s, v = cv2.split(hsv_frame)
    # plt.imshow(s)
    # # hist, bins = np.histogram(s)
    # # plt.hist(s, bins=bins)
    # print('avg_hue: ', np.ndarray.mean(h))
    # print('stddev_hue: ', np.ndarray.std(h))
    # print('avg_sat: ', np.ndarray.mean(s))
    # print('stddev_sat: ', np.ndarray.std(s))
    # plt.colorbar()
    # plt.show()
