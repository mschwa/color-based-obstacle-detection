"""
References
    - https://realpython.com/python-opencv-color-spaces/
    - https://rodrigoberriel.com/2014/11/opencv-color-spaces-splitting-channels/
    - https://stackoverflow.com/questions/42798659/how-to-remove-small-connected-objects-using-opencv/42812226
    - https://stackoverflow.com/questions/35854197/how-to-use-opencvs-connected-components-with-stats-in-python

Parking Lot
    - Try to use two separate masks for hue and saturation thresholding?
    - Preprocessing approach: calibration step with picture containing grass
      only could be a better way to set initial paramters;
"""

import os
import cv2
import numpy as np

# # define initial values for hue minimum and maximum thresholds along with
# # saturation minimum threshold
# h_min = 10     # iphone: 40
# h_max = 118    # iphone: 70
# s_min = 27     # iphone: 128
# # create HSV min and max thresholds noting that, in OpenCV, HSV channel values
# # have ranges from 0 to (180, 255, 255) rather than to (360, 1, 1)
# hsv_min = (h_min, s_min, 0)
# hsv_max = (h_max, s_max, 255)


def connected_components(thresholded_frame, size_threshold):
    # negate mask for connected component analysis
    thresholded_frame = cv2.bitwise_not(thresholded_frame)
    # find all connected components in mask and have each assigned an
    # integer label
    n_components, output, stats, centroids = \
        cv2.connectedComponentsWithStats(thresholded_frame, connectivity=8)
    # ignore background connected component in the number of connected
    # components and in the stats associated with each of them
    n_components -= 1
    sizes = stats[1:, -1]
    # create array to store (black and white) ouput image
    filtered_frame = np.full((output.shape), 255, dtype=np.uint8)
    # declare arrays that store (1) closest row containing (i+1)th
    # connected component and (2) column where this label was assigned
    rows_closest = []
    cols_closest = []
    # only keep components with size greater than threshold
    for i in range(0, n_components):
        if sizes[i] >= size_threshold:
            # get mask containing pixels of (i+1)th connected component.
            # +1 accounts for skipping foreground component
            m = (output == i + 1)
            # get rows and columns of all pixels in mask associated with
            # component
            rows, cols = np.where(m)
            # get closest row with pixel labeled as obstacle
            idx = np.argmax(rows)
            # store row and column of obstacle's pixel closest to camera
            rows_closest.append(rows[idx])
            cols_closest.append(cols[idx])
            # set component pixels black
            filtered_frame[m] = 0
    
    return filtered_frame, (rows_closest, cols_closest)


def segment(bgr_frame, hsv_min, hsv_max, size_threshold):
    """
    Inputs
    ------
        bgr_frame: ndarray
            BGR frame

    Returns
    -------
        ndarray
            Output of grass segmentation on frame
        tuple
            rows and corresponding columns of closest pixel
            of each detected obstacle
    """
    ### 1. Convert frame from BGR color space to HSV
    hsv_frame = cv2.cvtColor(bgr_frame, cv2.COLOR_BGR2HSV)

    ### 2. Segment using old parameters
    # threshold pixels: mask tells which pixels fall in the hsv range and
    # which don't
    grass_mask_init = cv2.inRange(hsv_frame, hsv_min, hsv_max)
    # apply preliminary mask to frame so as to keep only original pixes in the
    # range
    masked_frame = cv2.bitwise_and(hsv_frame, hsv_frame, mask=grass_mask_init)

    # ### 3. calculate statistics from segmentation
    # h, s, v = cv2.split(masked_frame)
    # avg_hue = np.ndarray.mean(h)
    # stddev_hue = np.ndarray.std(h)
    # avg_sat = np.ndarray.mean(s)
    # stddev_sat = np.ndarray.std(s)

    # ### 4. update parameters
    # # update threshold parameters with mean shift
    # h_min = avg_hue - 3 * stddev_hue
    # h_max = avg_hue + 3 * stddev_hue
    # s_min = avg_sat - 3 * stddev_sat
    # # use parameters to update range thresholds
    # hsv_min = (h_min, s_min, 0)
    # hsv_max = (h_max, 255, 255)

    ### 5. repeat segmentation with updated parameters
    grass_mask_final =  cv2.inRange(hsv_frame, hsv_min, hsv_max)

    ### 6. refine result with connected component analysis
    filtered_frame, (rows_closest, cols_closest) = connected_components(grass_mask_final, size_threshold)

    return filtered_frame, (rows_closest, cols_closest)

