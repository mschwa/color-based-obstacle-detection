"""
    Calling Ubuntu's Sox from Python to generate audio output
        - https://askubuntu.com/questions/920539/how-to-play-a-sound-from-terminal
        - https://stackoverflow.com/questions/63044141/sox-create-and-play-tone-only-on-left-or-right-channel
"""

import os
import cv2
import subprocess
import numpy as np
from time import sleep

from constants import FRAME_W, FRAME_H,\
                      LEFT_BOUND_COL, RIGHT_BOUND_COL,\
                      SILENT_BOUND_ROW, BEEP_BOUND_ROW

# play command in quiet mode (-q, no output generation) and using two channels
# (-c 2) for stereo sound
PLAY_CMD = 'play -q -c 2'
# two beep options: ubuntu generated signal(synth <durantion> sin <frequency>)
# and downloaded tone
SYNTH_CMD = '-n synth 0.2 sin 44100'
XYLO_CMD = './audio/xylophone-b-200ms.wav'
# left and rigth panning of audio
LEFT_PAN = 'remix 1 0'
RIGHT_PAN = 'remix 0 1'
BOTH_PAN = 'remix 1 1'
# full commands
LEFT_CMD = '{} {} {}'.format(PLAY_CMD, XYLO_CMD, LEFT_PAN)
BOTH_CMD = '{} {} {}'.format(PLAY_CMD, XYLO_CMD, BOTH_PAN)
RIGHT_CMD = '{} {} {}'.format(PLAY_CMD, XYLO_CMD, RIGHT_PAN)
NEUTRAL_CMD = ' true '


def beep_settings(closest_px):
    """
    Params
    ------
        closest_px: tuple
            (row, col)-coordinate of closest obstacle pixel
    
    Returns
    -------
        float
            interval between beeps
        string
            play command for audio
    """

    # retrieve frame height and witdth from environment
    # H, W = int(os.environ.get('H')), int(os.environ.get('W'))
    # use commands from global scope
    global LEFT_CMD, BOTH_CMD, RIGHT_CMD
    # declare rate and command variables
    rate = None
    cmd = None
    # get row and column of closest obstacle pixel
    row, col = closest_px
    # calculate rate in terms of pixel row
    if (row > SILENT_BOUND_ROW):
        if (row < BEEP_BOUND_ROW): rate = 0.4
        else:                      rate = 0.1
    # define beep generation command in terms of pixel column
    if   (col < LEFT_BOUND_COL):           cmd = LEFT_CMD
    elif (col < RIGHT_BOUND_COL):          cmd = BOTH_CMD
    else:                                  cmd = RIGHT_CMD
    
    return rate, cmd


def segmentation2audio(segmented_frame, obstacle_pixels):
    """
    Generate audio feedback from segmented frame.

    Params
    ------
        segmented_frame: ndarray
            result of grass segumentation
        obstacle pixels: list
            (row, col) coordinate of closest pixel of all obstacles

    Returns
    -------
        tuple
            beep rate and generation command for audio feedback
    """
    
    # retrieve frame height and witdth from environment
    # H, W = int(os.environ.get('H')), int(os.environ.get('W'))
    H, W = FRAME_H, FRAME_W
    # separate rows and columns from pixels of each obstacle
    rows_closest, cols_closest = obstacle_pixels
    # convert frame from grayscale to bgr (for visual tests)
    colored_frame = cv2.cvtColor(segmented_frame, cv2.COLOR_GRAY2BGR)
    # draw boundary lines separating frame's left, center and right regions
    cv2.line(colored_frame, (LEFT_BOUND_COL, 0), (LEFT_BOUND_COL, H), (0, 0, 255))
    cv2.line(colored_frame, (RIGHT_BOUND_COL, 0), (RIGHT_BOUND_COL, H), (0, 0, 255))
    # if obstacle exists in image
    if (len(rows_closest)):
        # declare size of red dot displayed at location of closest obstacle
        r = 10
        # get closest pixel obstacle in all image
        closest_idx = np.argmax(rows_closest)
        px = (rows_closest[closest_idx], cols_closest[closest_idx])
        # determine rate and play command for beep
        rate, cmd = beep_settings(px)
        # color region around pixel
        sq_top    = max(px[0] - r, 0)
        sq_bottom = min(px[0] + r, H)
        sq_left   = max(px[1] - r, 0)
        sq_right  = min(px[1] + r, W)
        colored_frame[sq_top:sq_bottom, sq_left:sq_right] = [0, 0, 255]
        
        return (rate, cmd), colored_frame
    else:
        return (0, NEUTRAL_CMD), colored_frame


def notification_routine(beep_process_input):
    global NEUTRAL_CMD
    # initialize rate and audio generation command
    rate = 0
    cmd = NEUTRAL_CMD
    
    while True:

        if(rate):
            # play audio in a non-blocking manner
            subprocess.check_output(cmd.split())
            # wait interval between beeps
            sleep(rate)
        
        # check if new rate/command have been input
        if (len(beep_process_input) == 0):
            continue

        # retrieve new rate/command
        rate, cmd = beep_process_input[0]
        # only empty array at the end of loop so that next rate/cmd received
        # are as recent as possible
        beep_process_input.pop()



### ATTEMPTS TO GENERATE LATERALIZED SOUNDS WITH OTHER LIBRARIES ##############



# from pydub import AudioSegment
# from pydub.playback import play
# from time import sleep

# beep = AudioSegment.from_file('./audio/xylophone-b-stereo.wav', format='wav', channels=2)
# beep_left = beep.pan(-1)
# beep_right = beep.pan(1)

# print('PLAYING LEFT...')
# play(beep_left)

# sleep(2)

# print('PLAYING RIGHT...')
# play(beep_right)



# import pygame
# from time import sleep

# pygame.mixer.init(channels=2)

# # beep = pygame.mixer.Sound('./audio/xylophone-b.wav')
# beep = pygame.mixer.Sound('./audio/xylophone-b-stereo.wav')
# # find channel to play audio
# channel_0 = pygame.mixer.find_channel()
# channel_1 = pygame.mixer.find_channel()

# # print('PLAYING LEFT...')
# # channel.set_volume(1, 0)
# # channel.play(beep)

# # sleep(2)

# print('PLAYING RIGHT...')
# channel_1.set_volume(0, 1)
# channel_1.play(beep)