import math
import json


# load the current calibration parameters
with open('calibration/calibration.json', 'r') as fh:
    calibration = json.load(fh)

CAM_IDX = calibration['cam_index']
# set threshold for connected components (number of pixels)
SIZE = calibration['segmentation_size_threshold']

# thresholding vars
LOW_H = calibration['colorspace']['low_h']
HIGH_H = calibration['colorspace']['high_h']
LOW_S = calibration['colorspace']['low_s']
HIGH_S = calibration['colorspace']['high_s']

# frame dimensions
FRAME_W = calibration['frame']['W']
FRAME_H = calibration['frame']['H']

# width of left and right windows within considered during processing
SIDES_W   = 0.3

# define boundaries of region not in mower's path which can thus be ignored
# during processing
PROCESSED_AREA_COL_START = calibration['roi_bounds']['left']
PROCESSED_AREA_COL_END   = calibration['roi_bounds']['right']
# calculate width of area in mower's path
PROCESSED_W = PROCESSED_AREA_COL_END - PROCESSED_AREA_COL_START
# calculate region boundaries within processed area
LEFT_BOUND_COL  = math.floor(SIDES_W * PROCESSED_W)
RIGHT_BOUND_COL = PROCESSED_W - math.floor(SIDES_W * PROCESSED_W)

# define long-, medium- and short-ranges (no beep, slow beep, fast beep)
# respectively
SILENT_BOUND_ROW = calibration['proximity_bounds']['top']
BEEP_BOUND_ROW   = calibration['proximity_bounds']['bottom']
