"""
    Code adapted from: https://docs.opencv2.org/3.4/da/d97/tutorial_threshold_inRange.html
"""
import os
import sys
import json
import cv2

from thresholding import ThresholdCalibrator
from roi import RoiCalibrator
from proximity import ProximityCalibrator

# load the current calibration parameters
with open('calibration.json', 'r') as fh:
    calibration = json.load(fh)

cam_index = calibration['cam_index']
cap = cv2.VideoCapture(cam_index)
if (cap is None or not cap.isOpened()):
    raise Exception('Can\'t open camera by index {}'.format(cam_index))


# update frame height and width used during calibration
calibration['frame']['W'] = int(cap.get(3))
calibration['frame']['H'] = int(cap.get(4))

calibrate_color = input('Calibrate color? [Y/N] ')
if (calibrate_color.lower() == 'y'):
    # initialize calibrator
    colorspace_calib = ThresholdCalibrator(cap)
    # calibrate colorspace
    print('SELECT HUE AND SATURATION THRESHOLDS AND PRESS \'ENTER\' WHEN DONE')
    low_H, high_H, low_S, high_S, size = colorspace_calib.calibrate()
    # update calibration variables in batch 
    calibration['colorspace']['low_h'] = low_H
    calibration['colorspace']['high_h'] = high_H
    calibration['colorspace']['low_s'] = low_S
    calibration['colorspace']['high_s'] = high_S
    calibration['segmentation_size_threshold'] = size

print('---------------------------------------')
calibrate_roi = input('Calibrate Mower Dimensions? [Y/N] ')
if (calibrate_roi.lower() == 'y'):
    # initialize calibrator
    roi_calib = RoiCalibrator(cap)
    # calibrate mower roi
    print('PLACE BARS ON LEFT AND RIGHT ENDS OF MOWER AND PRESS \'ENTER\' WHEN DONE')
    roi_start, roi_end = roi_calib.calibrate()
    calibration['roi_bounds']['left'] = roi_start
    calibration['roi_bounds']['right'] = roi_end

print('---------------------------------------')
# calibrate proximity zones (far, approaching, close)
calibrate_zones = input('Calibrate proximity zones? [Y/N] ')
if (calibrate_zones.lower() == 'y'):
    # initialize calibrator
    proximity_calib = ProximityCalibrator(cap)
    print ('DEFINE AREAS OF PROXIMITY: FAR (TOP, NO BEEPS), APPROACHING ' +
        '(MIDDLE, LOW FREQUENCY BEEPS) AND CLOSE (BOTTOM, HIGH FREQUENCY BEEPS)')
    print('PRESS \'ENTER\' WHEN DONE')
    horizon_1, horizon_2 = proximity_calib.calibrate()
    calibration['proximity_bounds']['top'] = horizon_1
    calibration['proximity_bounds']['bottom'] = horizon_2

# overwrite calibration json
with open('calibration.json', 'w') as fh:
    json.dump(calibration, fh)
print('** SETTINGS UPDATED. **')