"""
    Code adapted from: https://docs.opencv2.org/3.4/da/d97/tutorial_threshold_inRange.html
"""
import os
import sys
import json
import cv2

# import from parent folder
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from segment import connected_components
from preprocess import preprocess
sys.path.pop()

class ThresholdCalibrator():
   
    # set max H and S values in trackbars
    max_value_S = 255
    max_value_H = 360 // 2

    # define window names
    STREAM_WINDOW_NAME = 'Camera Stream - Thresholding'
    CALIBRATION_WINDOW_NAME = 'Calibration - Thresholding'

    # define trackbar labels
    LOW_H_NAME = 'Low H'
    LOW_S_NAME = 'Low S'
    HIGH_H_NAME = 'High H'
    HIGH_S_NAME = 'High S'
    SIZE_NAME = 'Obstacle Min Size'

    def __init__(self, cap):
        super().__init__()

        # store reference to video stream
        self.cap = cap

        # set initial low and high values in trackbar ranges
        self.h_trackbar_min = 0
        self.h_trackbar_max = self.max_value_H
        self.s_trackbar_min = 0
        self.s_trackbar_max = self.max_value_S
        self.size_min = 100
        # approximate reasonable range for threshold range
        self.size_max = int(cap.get(3)) * int(cap.get(4)) // 50

        # create trackbar cursors
        self.low_H = 0
        self.low_S = 0
        self.high_H = self.h_trackbar_max
        self.high_S = self.s_trackbar_max
        self.size = 100


    def _on_low_H_thresh_trackbar(self, val):
        # update trackbar preventing low value from exceeding high value
        self.low_H = min(self.high_H - 1, val)
        cv2.setTrackbarPos(self.LOW_H_NAME, self.CALIBRATION_WINDOW_NAME, self.low_H)

    def _on_high_H_thresh_trackbar(self, val):
        self.high_H = max(val, self.low_H + 1)
        cv2.setTrackbarPos(self.HIGH_H_NAME, self.CALIBRATION_WINDOW_NAME, self.high_H)

    def _on_low_S_thresh_trackbar(self, val):
        self.low_S = min(self.high_S - 1, val)
        cv2.setTrackbarPos(self.LOW_S_NAME, self.CALIBRATION_WINDOW_NAME, self.low_S)

    def _on_high_S_thresh_trackbar(self, val):
        self.high_S = max(val, self.low_S + 1)
        cv2.setTrackbarPos(self.HIGH_S_NAME, self.CALIBRATION_WINDOW_NAME, self.high_S)

    def _on_size_trackbar(self, val):
        # size threshold should be greater than 0
        self.size = max(self.size_min, val)
        cv2.setTrackbarPos(self.SIZE_NAME, self.CALIBRATION_WINDOW_NAME, self.size)


    def _setup_interface(self):

        # clean up
        cv2.destroyAllWindows()

        # create stream and calibration windows
        cv2.namedWindow(self.STREAM_WINDOW_NAME)
        cv2.namedWindow(self.CALIBRATION_WINDOW_NAME)

        # create trackbars
        cv2.createTrackbar(self.LOW_H_NAME, self.CALIBRATION_WINDOW_NAME, self.low_H, \
                           self.max_value_H, self._on_low_H_thresh_trackbar)
        cv2.createTrackbar(self.HIGH_H_NAME, self.CALIBRATION_WINDOW_NAME, self.high_H, \
                           self.max_value_H, self._on_high_H_thresh_trackbar)
        cv2.createTrackbar(self.LOW_S_NAME, self.CALIBRATION_WINDOW_NAME, self.low_S, \
                           self.max_value_S, self._on_low_S_thresh_trackbar)
        cv2.createTrackbar(self.HIGH_S_NAME, self.CALIBRATION_WINDOW_NAME, self.high_S, \
                           self.max_value_S, self._on_high_S_thresh_trackbar)
        cv2.createTrackbar(self.SIZE_NAME, self.CALIBRATION_WINDOW_NAME, self.size, \
                           self.size_max, self._on_size_trackbar)


    def calibrate(self):
        """
            Returns
            -------
                tuple: (low_h, high_h, low_s, high_s) parameters adjusted
                       during calibration
        """
        # create windows and trackbars
        self._setup_interface()

        while True:
            ret, frame = self.cap.read()
            if frame is None:
                break

            pre_frame = preprocess(frame)

            # convert frame to HSV and threshold it
            frame_HSV = cv2.cvtColor(pre_frame, cv2.COLOR_BGR2HSV)
            frame_threshold = cv2.inRange(frame_HSV,
                                         (self.low_H, self.low_S, 0),
                                         (self.high_H, self.high_S, 255))

            # apply connected component analysis to thresholded frame
            connected_frame, _ = connected_components(frame_threshold, self.size)
            
            # show frame in both windoes
            cv2.imshow(self.STREAM_WINDOW_NAME, frame)
            cv2.imshow(self.CALIBRATION_WINDOW_NAME, connected_frame)
            
            key = cv2.waitKey(30)
            # if adjustment is complete
            if key == 13:
                cv2.destroyAllWindows()
                # return adjusted colorspace settings
                return (self.low_H, self.high_H, self.low_S, self.high_S, self.size)
