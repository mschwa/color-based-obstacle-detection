"""
    Code adapted from: https://docs.opencv2.org/3.4/da/d97/tutorial_threshold_inRange.html
"""
import os
import sys
import json
import math
import cv2


class ProximityCalibrator():

    # define window names
    STREAM_WINDOW_NAME = 'Camera Stream - Proximity'
    CALIBRATION_WINDOW_NAME = 'Calibration - Proximity'

    # define trackbar labels
    UPPER_BOUNDARY_NAME = 'Top Boundary'
    LOWER_BOUNDARY_NAME = 'Bottom Boundary'

    def __init__(self, cap):
        super().__init__()

        # store reference to video stream
        self.cap = cap
        self.W = int(cap.get(3))
        H = int(cap.get(4))
        self.HALF_H = H // 2

        # set initial low and high values in trackbar ranges
        self.trackbar_horiz1_max = self.HALF_H
        self.trackbar_horiz2_max = self.HALF_H

        # declare trackbar cursors setting their initial position
        self.horiz1_offset = self.HALF_H // 2
        self.horiz2_offset = int(0.3 * self.HALF_H)

    def _on_left_trackbar(self, val):
        self.horiz1_offset = val
        cv2.setTrackbarPos(self.UPPER_BOUNDARY_NAME, self.CALIBRATION_WINDOW_NAME, self.horiz1_offset)

    def _on_right_trackbar(self, val):
        self.horiz2_offset = val
        cv2.setTrackbarPos(self.LOWER_BOUNDARY_NAME, self.CALIBRATION_WINDOW_NAME, self.horiz2_offset)


    def _setup_interface(self):

        # clean up
        cv2.destroyAllWindows()

        # create stream and calibration windows
        cv2.namedWindow(self.STREAM_WINDOW_NAME)
        cv2.namedWindow(self.CALIBRATION_WINDOW_NAME)

        # create trackbars
        cv2.createTrackbar(self.UPPER_BOUNDARY_NAME, self.CALIBRATION_WINDOW_NAME , self.horiz1_offset,\
                           self.trackbar_horiz1_max, self._on_left_trackbar)
        cv2.createTrackbar(self.LOWER_BOUNDARY_NAME, self.CALIBRATION_WINDOW_NAME , self.horiz2_offset, \
                           self.trackbar_horiz2_max, self._on_right_trackbar)


    def calibrate(self):
        """
            Returns
            -------
                tuple: (roi_start, roi_end) parameters adjusted during calibration
        """
        # create windows and trackbars
        self._setup_interface()

        txt_offset = 25
        while True:
            ret, frame = self.cap.read()
            if frame is None:
                break

            horiz1 = self.horiz1_offset
            horiz2 = self.HALF_H + self.horiz2_offset
            
            # show original frame
            cv2.imshow(self.STREAM_WINDOW_NAME, frame)
            # draw lines in calibration frame
            cv2.line(frame, (0, horiz1), (self.W, horiz1), (0, 0, 255), thickness=2)
            cv2.line(frame, (0, horiz2), (self.W, horiz2), (0, 0, 255), thickness=2)
            # TODO: print name of zones on: top, below 1st line and below 2nd line
            cv2.putText(frame, 'NO BEEP ZONE', (0, txt_offset), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 2)
            cv2.putText(frame, 'LOW FREQUENCY ZONE', (0, horiz1 + txt_offset), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 2)
            cv2.putText(frame, 'HIGH FREQUENCY ZONE', (0, horiz2 + txt_offset), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 2)
            cv2.imshow(self.CALIBRATION_WINDOW_NAME, frame)
            
            key = cv2.waitKey(30)
            if key == 13:
                cv2.destroyAllWindows()
                # return adjusted roi bounds
                return (horiz1, horiz2)


if __name__ == '__main__':
    cap = cv2.VideoCapture(0)
    calib = ProximityCalibrator(cap)
    calib.calibrate()
