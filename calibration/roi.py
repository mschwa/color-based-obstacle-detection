"""
    Code adapted from: https://docs.opencv2.org/3.4/da/d97/tutorial_threshold_inRange.html
"""
import os
import sys
import json
import math
import cv2


class RoiCalibrator():

    # define window names
    STREAM_WINDOW_NAME = 'Camera Stream - ROI'
    CALIBRATION_WINDOW_NAME = 'Calibration - ROI'

    # define trackbar labels
    LEFT_TRACK_NAME = 'Mower\'s Left Side'
    RIGHT_TRACK_NAME = 'Mower\'s Right Side'

    def __init__(self, cap):
        super().__init__()

        # store reference to video stream
        self.cap = cap
        W = int(cap.get(3))
        self.HALF_W = W // 2
        self.H = int(cap.get(4))

        # set initial low and high values in trackbar ranges
        self.trackbar_left_min = 0
        self.trackbar_left_max = self.HALF_W
        self.trackbar_right_min = 0
        self.trackbar_right_max = self.HALF_W

        # declare trackbar cursors
        self.left_offset = self.trackbar_left_min
        self.right_offset = self.trackbar_right_max

    def _on_left_trackbar(self, val):
        self.left_offset = val
        cv2.setTrackbarPos(self.LEFT_TRACK_NAME, self.CALIBRATION_WINDOW_NAME, self.left_offset)

    def _on_right_trackbar(self, val):
        self.right_offset = val
        cv2.setTrackbarPos(self.RIGHT_TRACK_NAME, self.CALIBRATION_WINDOW_NAME, self.right_offset)


    def _setup_interface(self):

        # clean up
        cv2.destroyAllWindows()

        # create stream and calibration windows
        cv2.namedWindow(self.STREAM_WINDOW_NAME)
        cv2.namedWindow(self.CALIBRATION_WINDOW_NAME)

        # create trackbars
        cv2.createTrackbar(self.LEFT_TRACK_NAME, self.CALIBRATION_WINDOW_NAME , self.left_offset,\
                           self.trackbar_left_max, self._on_left_trackbar)
        cv2.createTrackbar(self.RIGHT_TRACK_NAME, self.CALIBRATION_WINDOW_NAME , self.right_offset, \
                           self.trackbar_right_max, self._on_right_trackbar)


    def calibrate(self):
        """
            Returns
            -------
                tuple: (roi_start, roi_end) parameters adjusted during calibration
        """
        # create windows and trackbars
        self._setup_interface()

        while True:
            ret, frame = self.cap.read()
            if frame is None:
                break

            # define boundaries of region not in mower's path which can thus be ignored
            # during processing
            roi_start = self.left_offset
            roi_end   = self.HALF_W + self.right_offset
            
            # show original frame
            cv2.imshow(self.STREAM_WINDOW_NAME, frame)
            # draw lines in calibration frame
            cv2.line(frame, (roi_start, 0), (roi_start, self.H), (0, 0, 255), thickness=2)
            cv2.line(frame, (roi_end, 0), (roi_end, self.H), (0, 0, 255), thickness=2)
            cv2.imshow(self.CALIBRATION_WINDOW_NAME, frame)
            
            key = cv2.waitKey(30)
            if key == 13:
                cv2.destroyAllWindows()
                # return adjusted roi bounds
                return (roi_start, roi_end)