"""
References
    - https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_histograms/py_histogram_equalization/py_histogram_equalization.html
    - https://www.pyimagesearch.com/2015/10/05/opencv-gamma-correction/
    - https://stackoverflow.com/questions/46390779/automatic-white-balancing-with-grayworld-assumption/46391574
"""
import cv2
import numpy as np

# determine preprocessing parameters
CLIP_LIMIT = 1              # CLAHE clip limit
TILE_GRID_SIZE = (8, 8)     # CLAHE grid size
L_MULTIPLIER = 0.6          # L channel multiplies
GAMMA = 0.5                 # gamma in gamma correction

# create CLAHE (Contrast Limited Adaptive Histogram Equalization) object
# defining the clip limit and the size of the tiles for which local
# equalization will be performed.
clahe = cv2.createCLAHE(clipLimit=CLIP_LIMIT, tileGridSize=TILE_GRID_SIZE)


def gamma_correction(image, gamma):
    invGamma = 1.0 / gamma
    # map input pixel values to the output gamma corrected values
    table = np.array([((i / 255.0) ** invGamma) * 255
        for i in np.arange(0, 256)]).astype("uint8")
    # apply gamma correction using the mapping
    return cv2.LUT(image, table)



def saturation_correction(bgr_image, clipPercent = 0, hist_size = 256):
    hsv_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv_image)

    if (clipPercent == 0):
        # Finds the global minimum and maximum in an array. 
        min_s, max_s, min_loc, max_loc = cv2.minMaxLoc(s)
    else: 
        # calculate saturation histogram
        hist = cv2.calcHist(hsv_image, [1], None, [hist_size], (0, hist_size), accumulate=False)
        # accumulate histogram
        accumulator = [0] * hist_size
        for i in range(1, hist_size):
            accumulator[i] = hist[i] + accumulator[i-1]
        # make clip percentage absolute
        clipPercent *= accumulator[-1] / 100
        # account for left and right wings
        clipPercent /= 2
        # locate left cut
        min_s = 0
        while (accumulator[min_s] < clipPercent):
            min_s += 1
        # local right cut
        max_s = hist_size - 1
        while (accumulator[max_s] >= (accumulator[-1] - clipPercent)):
            max_s -= 1


def white_balance(img):
    """
    Inputs
    ------
    img: ndarray
        image in LAB color space.
    """
    avg_a = np.average(img[:, :, 1])
    avg_b = np.average(img[:, :, 2])
    img[:, :, 1] = img[:, :, 1] - ((avg_a - 128) * (img[:, :, 0] / 255.0) * 1.1)
    img[:, :, 2] = img[:, :, 2] - ((avg_b - 128) * (img[:, :, 0] / 255.0) * 1.1)
    return img


def preprocess(frame):
        # convert frame to LAB color-space
        lab_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2LAB) 
        # white balance frame for consistency across different cameras
        lab_frame = white_balance(lab_frame)
        # apply adaptive histogram equalization in L channel
        lab_frame[..., 0] = clahe.apply(lab_frame[..., 0])
        # scale down lightness channel to descrease brigthness
        # lab_frame[..., 0] = np.multiply(lab_frame[..., 0], L_MULTIPLIER)

        # convert to bgr
        bgr_frame = cv2.cvtColor(lab_frame, cv2.COLOR_LAB2BGR)
        # apply gamma correction
        bgr_frame = gamma_correction(bgr_frame, GAMMA)

        return bgr_frame